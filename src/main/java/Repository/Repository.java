package Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import domain.Entity;
import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;

public abstract class Repository <TEntity> implements IRepository<TEntity>, UnitOfWorkRepository {
	
	protected UnitOfWork unitOfWork;
	protected Connection connection;
	protected PreparedStatement selectById;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement count;
	protected PreparedStatement selectAll;


	protected String selectByIDSql= "SELECT * FROM " + getTableName() + " WHERE id=?";
	protected String deleteSql="DELETE FROM " + getTableName() + " WHERE id=?";
	protected String selectAllSql= "SELECT * FROM " + getTableName();
	protected String countSql = "SELECT COUNT(*) FROM" + getTableName();
	
	protected Repository(Connection connection, UnitOfWork unitOfWork)
	{
		this.unitOfWork=unitOfWork;
		this.connection = connection;
		try {
			selectById=connection.prepareStatement(selectByIDSql);
			insert = connection.prepareStatement(getInsertQuery());
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(getUpdateQuery());
			selectAll = connection.prepareStatement(selectAllSql);
			count = connection.prepareStatement(countSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract void setUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();

	public void persistAdd(Entity entity) {
		try {
			setInsertQuery((TEntity) (entity));
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void persistDelete(Entity entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void persistUpdate(Entity entity) {
		try {
			setUpdateQuery((TEntity)entity);
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void add(TEntity entity) {
		unitOfWork.markAsNew((Entity) entity, this);
	}

	public void delete(TEntity entity) {
		unitOfWork.markAsDeleted((Entity) entity, this);
		
	}

	public void modify(TEntity entity) {
		unitOfWork.markAsChanged((Entity) entity, this);
		
	}

	
	
}
