package Repository;

import java.sql.Connection;

import UnitOfWork.UnitOfWork;

public class HsqlRepositoryCatalog implements RepositoryCatalog {
	
	private Connection connection;
	private UnitOfWork unitOfWork;
	
	public HsqlRepositoryCatalog(Connection connection, UnitOfWork unitOfWork){
		this.connection = connection;
		this.unitOfWork = unitOfWork;
	}
	
		public EnumerationValueRepository enumerations() {
			return new HsqlEnumerationValuesRepository(this.connection, unitOfWork);
		}
	
		public UserRepository users() {
			return new HsqlUsersRepository(this.connection, unitOfWork);
		}
		
		public PersonRepository persons(){
			return new HsqlPersonRepository(connection, unitOfWork);
		}
		
		public AddressRepository addresses(){
			return new HsqlAddressRepository(connection, unitOfWork);
		}

}
