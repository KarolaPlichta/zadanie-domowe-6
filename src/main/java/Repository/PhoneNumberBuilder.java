package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.PhoneNumber;

public class PhoneNumberBuilder implements IBuilder<PhoneNumber>{

	public PhoneNumber build(ResultSet result) throws SQLException {
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setId(result.getInt("Id"));
		phoneNumber.setCountryPrefix(result.getString("CountryPrefix"));
		phoneNumber.setCityPrefix(result.getString("CityPrefix"));
		phoneNumber.setNumber(result.getString("Number"));
		phoneNumber.setTypeId(result.getInt("TypeId"));
		return phoneNumber;
	}

}
