package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Person;

public class PersonBuilder implements IBuilder<Person> {

	public Person build(ResultSet result) throws SQLException {
		Person person = new Person();
		person.setFirstName(result.getString("firstName"));
		person.setSurname(result.getString("surName"));
		person.setPesel(result.getString("pesel"));
		person.setNip(result.getString("nip"));
		person.setEmail(result.getString("email"));
		person.setDateOfBirth(result.getDate("dateOfBirth"));
		return person;
	}

}
