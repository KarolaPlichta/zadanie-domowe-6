package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UnitOfWork.UnitOfWork;
import domain.Person;

public class HsqlPersonRepository extends Repository<Person> implements PersonRepository{
	
	private Statement statement;

	public HsqlPersonRepository(Connection connection, UnitOfWork unitOfWork) {
		super(connection, unitOfWork);
	}

	public List<Person> allOnPage(PagingInfo page) {
		List<Person> peoplelist= new ArrayList<Person>();
		int count = 0;
		  try {
			  	
	            ResultSet result = selectAll.executeQuery();
	            while(result.next()){
	            	PersonBuilder personBuild = new PersonBuilder();
	            	peoplelist.add(personBuild.build(result));
	             }
	            count = this.count();
	            page.setTotalCount(count);
	            page.setPage(1);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return peoplelist;
	}

	public void add(Person entity) {
		RoleChecker<Person> roleChecker = new RoleChecker<Person>();
		
		 try {
			 	if(roleChecker.check(entity).contains(RuleResult.Error)){
			 		CheckResult checkerResult = new CheckResult("Information inccorrect", RuleResult.Error);
			 	}
			 	else{
			 	this.setInsertQuery(entity);
			 	statement.execute(getInsertQuery());
			 	} 
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		
	}

	public void delete(Person entity) {
		try {
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(Person entity) {
		 try {
			 	this.setUpdateQuery(entity);
			 	statement.execute(getUpdateQuery());
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		
	}

	public int count() {
		int countPerson =0;
		try {
			
			countPerson = count.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countPerson;
	}

	public Person withId(int id) {
		Person person = new Person();
	
			try {
				selectById.setInt(1, id);
				ResultSet result = selectById.executeQuery();
				PersonBuilder personBuild = new PersonBuilder();
            	person = personBuild.build(result);
			 } catch (SQLException e) {
		            e.printStackTrace();
		          
		        }
		return person;
	
	}

	public Person withPesel(int pesel) {
		Person person = new Person();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Person WHERE pesel = " + pesel + ");");
	            while(result.next()){
	            	PersonBuilder personBuild = new PersonBuilder();
	            	person = personBuild.build(result);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return person;
	}

	public List<Person> withSurname(String surname) {
		List<Person> peopleList= new ArrayList<Person>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Person WHERE surname = " + surname + ");");
	            while(result.next()){
	            	PersonBuilder personBuild = new PersonBuilder();
	            	peopleList.add(personBuild.build(result));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return peopleList;
	}

	public List<Person> withSurnameAndName(String surname, String name) {
		List<Person> peopleList= new ArrayList<Person>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Person WHERE surname = " + surname + " AND name= " + name + ");");
	            while(result.next()){
	            	PersonBuilder personBuild = new PersonBuilder();
	            	peopleList.add(personBuild.build(result));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return peopleList;
	}

	@Override
	protected void setUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2,entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getNip());
		update.setString(5, entity.getEmail());
		update.setDate(6, (Date) entity.getDateOfBirth());
		update.setInt(7, entity.getId());
	}

	@Override
	protected void setInsertQuery(Person entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2,entity.getSurname());
		insert.setString(3, entity.getPesel());
		insert.setString(4, entity.getNip());
		insert.setString(5, entity.getEmail());
		insert.setDate(6, (Date) entity.getDateOfBirth());
		
	}

	@Override
	protected String getTableName() {
		return "Person";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE Person SET (firstName, surname, pesel, nip, email, dateOfBirth)=(?,?,?,?,?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Person (firstName, surname, pesel, nip, email, dateOfBirth) VALUES(?,?,?,?,?,?)";
	}

}
