package Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UnitOfWork.UnitOfWork;
import domain.Address;

public class HsqlAddressRepository extends Repository<Address> implements AddressRepository{
	
	private Statement statement;

	protected HsqlAddressRepository(Connection connection, UnitOfWork unitOfWork) {
		super(connection, unitOfWork);

	}

	public Address withId(int id) {
		Address address = new Address();
		AddressBuilder addressBuilder = new AddressBuilder();
		try {
			selectById.setInt(1, id);
            ResultSet result = selectById.executeQuery();
            address = addressBuilder.build(result);
		}
         catch(SQLException e){
            e.printStackTrace();
          
        }
		return address;
	}

	public List<Address> allOnPage(PagingInfo page) {
		List<Address> addressList= new ArrayList<Address>();
		int count = 0;
		  try {
			  	
	            ResultSet result = selectAll.executeQuery();
	            while(result.next()){
	            	AddressBuilder addressBuilder = new AddressBuilder();
	            	addressList.add(addressBuilder.build(result));
	             }
	            count = this.count();
	            page.setTotalCount(count);
	            page.setPage(1);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return addressList;
	}
	
	public void add(Address entity) {
		 try {
			 	this.setInsertQuery(entity);
			 	statement.execute(getInsertQuery());
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
	}

	public void delete(Address entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(Address entity) {
		try {
			this.setUpdateQuery(entity);
			statement.execute(getUpdateQuery());
       } catch (SQLException e) {
           e.printStackTrace();
         
       }

	}

	public int count() {
		int countAddress = 0;
		try {
			countAddress = count.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countAddress;
	}

	public List<Address> withCountryId(int id) {
		List<Address> addressList= new ArrayList<Address>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Address WHERE CountryId = " + id + ");");
	            while(result.next()){
	            	AddressBuilder addressBuilder = new AddressBuilder();
	            	addressList.add(addressBuilder.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return addressList;
	}

	public List<Address> withZipCode(String zipCode) {
		List<Address> addressList= new ArrayList<Address>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Address WHERE ZipCode = " + zipCode + ");");
	            while(result.next()){
	            	AddressBuilder addressBuilder = new AddressBuilder();
	            	addressList.add(addressBuilder.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return addressList;
	}

	public List<Address> withCountryIdAndCity(int id, String city) {
		List<Address> addressList= new ArrayList<Address>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM Address WHERE CountryId = " + id + " AND City = " + city + ");");
	            while(result.next()){
	            	AddressBuilder addressBuilder = new AddressBuilder();
	            	addressList.add(addressBuilder.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return addressList;
	}

	@Override
	protected void setUpdateQuery(Address entity) throws SQLException {
		update.setInt(1,entity.getCountryId());
		update.setInt(2, entity.getRegionId());
		update.setString(3,entity.getCity());
		update.setString(4, entity.getStreet());
		update.setString(5, entity.getHouseNumber());
		update.setString(6,entity.getLocalNumber());
		update.setString(7, entity.getZipCode());
		update.setInt(8, entity.getTypeId());
		update.setInt(9, entity.getId());
		
	}

	@Override
	protected void setInsertQuery(Address entity) throws SQLException {
		insert.setInt(1,entity.getCountryId());
		insert.setInt(2, entity.getRegionId());
		insert.setString(3,entity.getCity());
		insert.setString(4, entity.getStreet());
		insert.setString(5, entity.getHouseNumber());
		insert.setString(6,entity.getLocalNumber());
		insert.setString(7, entity.getZipCode());
		insert.setInt(8, entity.getTypeId());
		
	}

	@Override
	protected String getTableName() {
		return "Address";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE Address SET(CountryId, RegionId, City, Street, HouseNumber, LocalNumber, ZipCode, TypeId) = (?, ?, ?, ?, ?, ?, ?, ?,) WHERE Id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Address (CountryId, RegionId, City, Street, HouseNumber, LocalNumber, ZipCode, TypeId) VALUES (?, ?, ?, ?, ?, ?, ?, ?,)";
	}

}
