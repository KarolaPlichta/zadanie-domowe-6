package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.EnumerationValue;

public class EnumerationValueBuilder implements IBuilder<EnumerationValue> {

	public EnumerationValue build(ResultSet result) throws SQLException {
		EnumerationValue enumerationValue = new EnumerationValue();
		enumerationValue.setId(result.getInt("Id"));
		enumerationValue.setIntKey(result.getInt("IntKey"));
        enumerationValue.setStringKey(result.getString("StringKey"));
        enumerationValue.setValue(result.getString("Value"));
        enumerationValue.setEnumerationName(result.getString("EnumerationName"));
		return enumerationValue;
	}

}
