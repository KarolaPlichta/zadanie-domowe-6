package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Entity;

public interface IBuilder <TEntity extends Entity>{
	
	public TEntity build(ResultSet resultSet) throws SQLException;

}
