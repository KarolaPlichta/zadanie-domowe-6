package Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UnitOfWork.UnitOfWork;
import domain.EnumerationValue;

public class HsqlEnumerationValuesRepository extends Repository<EnumerationValue> implements EnumerationValueRepository {
	
	private Statement statement;
	
	public HsqlEnumerationValuesRepository(Connection connection, UnitOfWork unitOfWork){
		super(connection, unitOfWork);
	}

	public EnumerationValue withId(int id) {
		  EnumerationValue enumerationValue = new EnumerationValue();
		  EnumerationValueBuilder enumerationValueBuild = new EnumerationValueBuilder();
			try {
				selectById.setInt(1, id);
	            ResultSet result = selectById.executeQuery();
	            enumerationValue = enumerationValueBuild.build(result);
			}
	         catch(SQLException e){
	            e.printStackTrace();
	          
	        }
		return enumerationValue;
	}

	public void add(EnumerationValue entity) {
		 try {
			 	this.setInsertQuery(entity);
			 	statement.execute(getInsertQuery());
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
	}

	public void delete(EnumerationValue entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(EnumerationValue entity) {
		try {
			this.setUpdateQuery(entity);
			statement.execute(getUpdateQuery());
        } catch (SQLException e) {
            e.printStackTrace();
          
        }

	}

	public int count() {
		int countEnumeration = 0;
		try {
			countEnumeration = count.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countEnumeration;
	}

	public List<EnumerationValue> withName(String name) {
		List<EnumerationValue> elist= new ArrayList<EnumerationValue>();
		 try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM EnumerationValue WHERE EnumerationName = " + name + ");");
	            while(result.next()){
	            	EnumerationValueBuilder enumerationValueBuild = new EnumerationValueBuilder();
	            	elist.add(enumerationValueBuild.build(result));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return elist;
	}

	public List<EnumerationValue> withIntKey(int key, String name) {
		List<EnumerationValue> elist= new ArrayList<EnumerationValue>();
		  try {
	            ResultSet result = statement.executeQuery("Select * From EnumerationValue where IntKey =" + key + "AND EnumerationName = " + name + ";");
	            while(result.next()){
	            	EnumerationValueBuilder enumerationValueBuild = new EnumerationValueBuilder();
	            	elist.add(enumerationValueBuild.build(result));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return elist;
	}

	public List<EnumerationValue> withStringKey(String key, String name) {
		List<EnumerationValue> elist= new ArrayList<EnumerationValue>();
		  try {
	            ResultSet result = statement.executeQuery("Select * From EnumerationValue where StringKey =" + key + "AND EnumerationName = " + name + ";");
	            while(result.next()){
	            	EnumerationValueBuilder enumerationValueBuild = new EnumerationValueBuilder();
	            	elist.add(enumerationValueBuild.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return elist;
	}

	public List<EnumerationValue> allOnPage(PagingInfo page) {
		List<EnumerationValue> elist= new ArrayList<EnumerationValue>();
		int count = 0;
		  try {
			  	
	            ResultSet result = selectAll.executeQuery();
	            while(result.next()){
	            	EnumerationValueBuilder enumerationValueBuild = new EnumerationValueBuilder();
	            	elist.add(enumerationValueBuild.build(result));
	             }
	            count = this.count();
	            page.setTotalCount(count);
	            page.setPage(1);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return elist;
	}



	@Override
	protected void setUpdateQuery(EnumerationValue entity) throws SQLException {
		update.setInt(1, entity.getIntKey());
		update.setString(2, entity.getStringKey());
		update.setString(3, entity.getValue());
		update.setString(4, entity.getEnumerationName());
		update.setInt(5, entity.getId());
		
	}

	@Override
	protected void setInsertQuery(EnumerationValue entity) throws SQLException {
		insert.setInt(1, entity.getIntKey());
		insert.setString(2, entity.getStringKey());
		insert.setString(3, entity.getEnumerationName());
		insert.setString(4, entity.getValue());
	 	
		
	}

	@Override
	protected String getTableName() {
		return "EnumerationValue";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE EnumerationValue SET(intKey, stringKey, enumerationName, Value) = (?, ?, ?, ?) WHERE Id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO EnumerationValue(intKey, stringKey, enumerationName, Value) VALUES (?, ?, ?, ?)" ;
	}


}
