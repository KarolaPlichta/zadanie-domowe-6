package Repository;

import java.util.List;

import domain.Person;

public interface PersonRepository extends IRepository<Person> {
	public Person withPesel(int pesel);
	public List<Person> withSurname(String surname);
	public List<Person> withSurnameAndName(String surname, String name);

}
