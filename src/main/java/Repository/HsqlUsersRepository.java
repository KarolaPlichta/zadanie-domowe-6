package Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UnitOfWork.UnitOfWork;
import domain.RolesPermissions;
import domain.User;
import domain.UserRoles;

public class HsqlUsersRepository extends Repository<User> implements UserRepository {
	
	private Statement statement;
	private PreparedStatement roleUserStmt;
	private PreparedStatement permissionUserStmt;
	
	public HsqlUsersRepository(Connection connection, UnitOfWork unitOfWork){
		super(connection, unitOfWork);
		try {

			roleUserStmt = connection.prepareStatement("INSERT INTO Role (UserId, RoleId) VALUES (?, ?);");
			permissionUserStmt = connection.prepareStatement("INSERT INTO Permission (RoleId, PermissionId) VALUES (?, ?);");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public User withId(int id) {
		  User user = new User();
		  UserBuilder userBuild = new UserBuilder();
			try {
				selectById.setInt(1, id);
	            ResultSet result = selectById.executeQuery();
            	user = userBuild.build(result);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return user;
	}

	public List<User> allOnPage(PagingInfo page) {
		List<User> usersList= new ArrayList<User>();
		  int count = 0;
		  try {
			  	
	            ResultSet result = selectAll.executeQuery();
	            while(result.next()){
	            	UserBuilder user = new UserBuilder();
	            	usersList.add(user.build(result));
	            }
	            count = this.count();
	            page.setTotalCount(count);
	            page.setPage(1);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return usersList;
	}

	public void add(User entity) {
		RoleChecker<User> roleChecker = new RoleChecker<User>();
		
		 try {
			 	if(roleChecker.check(entity).contains(RuleResult.Error)){
			 		CheckResult checkerResult = new CheckResult("Information inccorrect", RuleResult.Error);
			 	}
			 	else{
			 	this.setInsertQuery(entity);
			 	statement.execute(getInsertQuery());
			 	}
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
	}

	public void delete(User entity) {
		try {
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(User entity) {
		 try {
			 	this.setUpdateQuery(entity);
			 	statement.execute(getUpdateQuery());
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
	}

	public int count() {
		int countUser =0;
		try {
			
			countUser = count.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countUser;
		
	}

	public List<User> withLogin(String login) {
		List<User> usersList= new ArrayList<User>();
		  try {
	            ResultSet result = statement.executeQuery("SELECT * FROM User WHERE Login = " + login + ");");
	            while(result.next()){
	            	UserBuilder user = new UserBuilder();
	            	usersList.add(user.build(result));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return usersList;
	}

	public User withLogginAndPassword(String login, String password) {
		ResultSet result = null;
		UserBuilder userBuild = new UserBuilder();
		User user = new User();
		  try {
			  	result = statement.executeQuery("SELECT * FROM User WHERE Login = " + login + "AND Password = " + password + ");");
	            user = userBuild.build(result);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return user;
	}
	

	public void setupPermissions(UserRoles user, RolesPermissions role) {
		try{
			roleUserStmt.setString(1, user.getUserId());
			roleUserStmt.setString(2, user.getRoleId());
			roleUserStmt.executeQuery();
			permissionUserStmt.setString(1,role.getRoleId());
			permissionUserStmt.setString(2,role.getPermissionId());
			permissionUserStmt.executeQuery();
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}



	@Override
	protected void setUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());
		
	}


	@Override
	protected void setInsertQuery(User entity) throws SQLException {
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
		
	}


	@Override
	protected String getTableName() {
		return "User";
	}


	@Override
	protected String getUpdateQuery() {
		return "UPDATE User SET (login,password)=(?,?) WHERE id=?";
	}


	@Override
	protected String getInsertQuery() {
		return "INSERT INTO User (login,password) VALUES(?,?)";
	}
	 	
		
	}

