package Repository;
import java.util.List;

import domain.EnumerationValue;

public interface EnumerationValueRepository extends IRepository<EnumerationValue> {
	public List<EnumerationValue> withName(String name);
	public List<EnumerationValue> withIntKey(int key, String name);
	public List<EnumerationValue> withStringKey(String key, String name);

}
