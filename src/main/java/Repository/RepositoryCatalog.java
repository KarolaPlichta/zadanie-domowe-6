package Repository;

public interface RepositoryCatalog {
	public EnumerationValueRepository enumerations();
	public UserRepository users();
}
