package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Address;

public class AddressBuilder implements IBuilder<Address> {

	public Address build(ResultSet result) throws SQLException {
		Address address = new Address();
		address.setId(result.getInt("Id"));
		address.setCountryId(result.getInt("CountryId"));
		address.setRegionId(result.getInt("RegionId"));
		address.setCity(result.getString("City"));
		address.setStreet(result.getString("Street"));
		address.setHouseNumber(result.getString("HouseNumber"));
		address.setLocalNumber(result.getString("LocalNumber"));
		address.setZipCode(result.getString("ZipCode"));
		address.setTypeId(result.getInt("TypeId"));
		return address;
	}

}
