package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.User;

public class UserBuilder implements IBuilder<User> {

	public User build(ResultSet result) throws SQLException {
		User user = new User();
		user.setId(result.getInt("id"));
		user.setLogin(result.getString("login"));
		user.setPassword(result.getString("password"));
		return user;
		
	}

}
