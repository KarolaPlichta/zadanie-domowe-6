package Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import UnitOfWork.UnitOfWork;
import domain.PhoneNumber;

public class HsqlPhoneNumberRepository extends Repository<PhoneNumber> implements PhoneNumberRepository{
	
	private Statement statement;

	protected HsqlPhoneNumberRepository(Connection connection, UnitOfWork unitOfWork) {
		super(connection, unitOfWork);
	}

	public PhoneNumber withId(int id) {
		PhoneNumber number = new PhoneNumber();
		PhoneNumberBuilder numberBuilder = new PhoneNumberBuilder();
		try {
			selectById.setInt(1, id);
            ResultSet result = selectById.executeQuery();
            number = numberBuilder.build(result);
		}
         catch(SQLException e){
            e.printStackTrace();
          
        }
		return number;
	}

	public List<PhoneNumber> allOnPage(PagingInfo page) {
		List<PhoneNumber> numberList= new ArrayList<PhoneNumber>();
		int count = 0;
		  try {
			  	
	            ResultSet result = selectAll.executeQuery();
	            while(result.next()){
	            	PhoneNumberBuilder numberBuilder = new PhoneNumberBuilder();
	            	numberList.add(numberBuilder.build(result));
	             }
	            count = this.count();
	            page.setTotalCount(count);
	            page.setPage(1);
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return numberList;
	}
	
	public void add(PhoneNumber entity) {
		 try {
			 	this.setInsertQuery(entity);
			 	statement.execute(getInsertQuery());
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
	}

	public void delete(PhoneNumber entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(PhoneNumber entity) {
		try {
			this.setUpdateQuery(entity);
			statement.execute(getUpdateQuery());
      } catch (SQLException e) {
          e.printStackTrace();
        
      }

	}

	public int count() {
		int countNumber = 0;
		try {
			countNumber = count.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countNumber;
	}

	public List<PhoneNumber> withCountryPrefix(String prefix) {
		List<PhoneNumber> numberList= new ArrayList<PhoneNumber>();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM PhoneNumber WHERE CountryPrefix = " + prefix +");");
	            while(result.next()){
	            	PhoneNumberBuilder numberBuilder = new PhoneNumberBuilder();
	            	numberList.add(numberBuilder.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return numberList;
	}

	public PhoneNumber withNumber(String number) {
		PhoneNumber phoneNumber = new PhoneNumber();
		  try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM PhoneNumber WHERE Number = " + number +");");
	            while(result.next()){
	            	PhoneNumberBuilder numberBuilder = new PhoneNumberBuilder();
	            	phoneNumber = numberBuilder.build(result);
	            	
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return phoneNumber;
	}

	public List<PhoneNumber> withCountryAndCityPrefix(String countryPrefix, String cityPrefix) {
		List<PhoneNumber> numberList= new ArrayList<PhoneNumber>();
		try {
			  	
	            ResultSet result = statement.executeQuery("SELECT * FROM PhoneNumber WHERE CountryPrefix = " + countryPrefix +" AND CityPrefix = " + cityPrefix + ");");
	            while(result.next()){
	            	PhoneNumberBuilder numberBuilder = new PhoneNumberBuilder();
	            	numberList.add(numberBuilder.build(result));
	             }
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return numberList;
		
	}

	@Override
	protected void setUpdateQuery(PhoneNumber entity) throws SQLException {
		update.setString(1, entity.getCountryPrefix());
		update.setString(2, entity.getCityPrefix());
		update.setString(3, entity.getNumber());
		update.setInt(4, entity.getTypeId());
		update.setInt(5, entity.getId());
	}

	@Override
	protected void setInsertQuery(PhoneNumber entity) throws SQLException {
		insert.setString(1, entity.getCountryPrefix());
		insert.setString(2, entity.getCityPrefix());
		insert.setString(3, entity.getNumber());
		insert.setInt(4, entity.getTypeId());
		
	}

	@Override
	protected String getTableName() {
		return "PhoneNumber";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE PhoneNumber SET(CountyPrefix, CityPrefix, Number, TypeId) = (?, ?, ?, ?) WHERE Id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO PhoneNumber (CountyPrefix, CityPrefix, Number, TypeId) VALUES (?, ?, ?, ?)";
	}

}
