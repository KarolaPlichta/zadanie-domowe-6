package Repository;

import java.util.List;

import domain.RolesPermissions;
import domain.User;
import domain.UserRoles;

public interface UserRepository extends IRepository<User>{
	public List<User> withLogin(String login);
	public User withLogginAndPassword(String login, String password);
	public void setupPermissions(UserRoles user, RolesPermissions role);

}
