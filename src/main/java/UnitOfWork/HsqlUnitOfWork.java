package UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.Entity;
import domain.EntityState;

public class HsqlUnitOfWork implements UnitOfWork{
	
	private Map<Entity, UnitOfWorkRepository> entities = new LinkedHashMap<Entity, UnitOfWorkRepository>();
	private Connection connection;

	
	public HsqlUnitOfWork(Connection connection){
		this.connection = connection;
	}
	
	public void saveChanges(Entity entity, UnitOfWorkRepository repo) {
		for(Entity entity1: entities.keySet())
		{
			switch(entity1.getState())
			{
			case Modified:
				entities.get(entity1).persistUpdate(entity1);
				break;
			case Deleted:
				entities.get(entity1).persistDelete(entity1);
				break;
			case New:
				entities.get(entity1).persistAdd(entity1);
				break;
			case UnChanged:
				break;
			case Unknown:
				break;}
		}
		
		try {
			connection.commit();;
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void undo() {
		entities.clear();
		
	}

	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.New);
		entities.put(entity, repo);
		
	}

	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repo);
		
		
	}

	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Modified);
		entities.put(entity, repo);
		
		
	}

}
