package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PersonFirstNameRule implements ICanCheckRule<Person> {

	
	public CheckResult checkRule(Person entity) {
		if(entity.getFirstName()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getFirstName().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		
	return new CheckResult("Fine", RuleResult.Ok);
		

	}
	

}
