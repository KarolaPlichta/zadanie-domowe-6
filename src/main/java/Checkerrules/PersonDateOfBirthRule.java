package Checkerrules;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PersonDateOfBirthRule implements ICanCheckRule<Person>{
	
	private Date date;

	public CheckResult checkRule(Person entity) {
		if(entity.getDateOfBirth()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getDateOfBirth().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(CheckIfDateMatchPesel(entity) == false){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		
		return new CheckResult("Fine", RuleResult.Ok);
	}
	private boolean CheckIfDateMatchPesel(Person entity){
		int beginYear = 0;
		long year;
		long dateOfBirth;
		Date date = null;
		boolean isTrue = false;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		beginYear = Integer.parseInt(entity.getPesel().substring(2, 4));
		if(beginYear <13){
			year = (1900 + Integer.parseInt(entity.getPesel().substring(0, 2)))*10000;
			dateOfBirth = year+beginYear*100+Integer.parseInt(entity.getPesel().substring(4, 6));
			try {
				date = df.parse(String.valueOf(dateOfBirth));
				this.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			isTrue = (this.getDate().equals(entity.getDateOfBirth()));
		}
		else if(beginYear<33 && beginYear>20){
			year = (2000 + Integer.parseInt(entity.getPesel().substring(0, 2)))*10000;
			dateOfBirth = year+(beginYear-20)*100+Integer.parseInt(entity.getPesel().substring(4, 6));
			try {
				date = df.parse(String.valueOf(dateOfBirth));
				this.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			isTrue = (this.getDate().equals(entity.getDateOfBirth()));
		}
		return isTrue;
		
		
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
