package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.User;


public class UserLoginRule implements ICanCheckRule<User> {

	public CheckResult checkRule(User entity) {
		if(entity.getLogin()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getLogin().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}

		return new CheckResult("Fine", RuleResult.Ok);
	
	}

}
