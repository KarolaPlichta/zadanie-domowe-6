package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Repository.HsqlEnumerationValuesRepository;
import Repository.HsqlUsersRepository;
import Repository.PagingInfo;
import UnitOfWork.HsqlUnitOfWork;
import UnitOfWork.UnitOfWork;

public class App 
{
    public static void main( String[] args )
    {
    	Connection connection;
    	UnitOfWork unitOfWork;

    	String url = "jdbc:hsqldb:hsql://localhost/workdb";
    	
    		try {
    	        connection = DriverManager.getConnection(url);
    	        unitOfWork = new HsqlUnitOfWork(connection);
    	        HsqlEnumerationValuesRepository enumerationValue = new HsqlEnumerationValuesRepository(connection, unitOfWork);
    	        HsqlUsersRepository user = new HsqlUsersRepository(connection, unitOfWork);
    	        PagingInfo page = new PagingInfo();
    	        enumerationValue.allOnPage(page);
    	        user.allOnPage(page);
    	        
    	    } catch (SQLException e) {
    	        System.err.println("Problem z otwarciem polaczenia");
    	        e.printStackTrace();
    	    }
    		
    		
    	}
    }
