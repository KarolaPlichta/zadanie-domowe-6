package domain;

public class PhoneNumber extends Entity{
	
	private String countryPrefix;
	private String cityPrefix;
	private String number;
	private int typeId;
	
	public String getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public String getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

}
