package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Checkerrules.PersonEmailRule;
import checker.CheckResult;
import checker.RuleResult;
import domain.Person;

public class PersonEmailTest {
	
	PersonEmailRule rule = new PersonEmailRule();

	@Test
	public void checker_should_check_if_the_person_email_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_email_is_not_empty(){
		Person p = new Person();
		p.setEmail("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_email_contains_figures(){
		Person p = new Person();
		p.setEmail("fhuesu");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void checker_should_return_Ok_if_the_email_is_correct(){
		Person p = new Person();
		p.setEmail("fhuesu@dsa.sd");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	

}
